//
//  Ci_GameData.m
//  Circles
//
//  Created by Alexis on 02/10/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import "Ci_GameData.h"

@implementation Ci_GameData

static NSString* const SSGameDataHighScoreKey = @"highScore";

+ (instancetype)sharedInstance {
	static Ci_GameData* _sharedInstance = nil;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_sharedInstance = [self loadInstance];
	});
	
	return _sharedInstance;
}
+(NSString*)filePath	{
	static NSString* filePath = nil;
	if (!filePath) {
		filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]
					 stringByAppendingPathComponent:@"gamedata"];
	}
	return filePath;
}
+(instancetype)loadInstance	{
	NSData* decodedData = [NSData dataWithContentsOfFile: [Ci_GameData filePath]];
	if (decodedData) {
		Ci_GameData* gameData = [NSKeyedUnarchiver unarchiveObjectWithData:decodedData];
		return gameData;
	}
	
	return [[Ci_GameData alloc] init];
}

-(void)save	{
	NSData* encodedData = [NSKeyedArchiver archivedDataWithRootObject: self];
	[encodedData writeToFile:[Ci_GameData filePath] atomically:YES];
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder	{
	self = [self init];
	if (self)	{
		_highScore = [aDecoder decodeDoubleForKey:SSGameDataHighScoreKey];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder	{
	[aCoder encodeDouble:_highScore forKey:SSGameDataHighScoreKey];
}

@end
