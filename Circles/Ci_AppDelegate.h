//
//  Ci_AppDelegate.h
//  Circles
//
//  Created by Alexis on 09/09/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ci_AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
