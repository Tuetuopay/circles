//
//  Vec2.h
//  Circles
//
//  Created by Alexis on 25/09/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#ifndef _VEC_2_H
#define _VEC_2_H

#include <iostream>

#include <CoreGraphics/CoreGraphics.h>

class Vec2	{
public:
	Vec2 ();
	Vec2 (double x, double y);
	Vec2 (CGPoint vec);
	Vec2 (CGPoint from, CGPoint to);
	~Vec2 ();
	
	double dot (const Vec2& vec) const;
	double cross (const Vec2& vec) const;
	double length ();
	Vec2 normalized () const;
	void normalize ();
	CGPoint toCGPoint () const;
	
	Vec2 operator+ (const Vec2& v) const;
	Vec2 operator- (const Vec2& v) const;
	Vec2 operator* (const double& v) const;
	Vec2 operator/ (const double& v) const;
	void operator+= (const Vec2& v);
	void operator-= (const Vec2& v);
	void operator*= (const double& v);
	void operator/= (const double& v);
	
	double x, y;
	
private:
	
};

#endif /* defined(_VEC_2_H) */