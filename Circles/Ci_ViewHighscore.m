//
//  Ci_ViewHighscore.m
//  Circles
//
//  Created by Alexis on 05/10/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import "Ci_ViewHighscore.h"

#import <QuartzCore/QuartzCore.h>

#import "Ci_ViewController.h"

@implementation Ci_ViewHighscore

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		//NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ViewNewHighscore" owner:self options:nil];
		//UIView *mainView = [subviewArray objectAtIndex:0];
		//[self addSubview:mainView];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder	{
	self = [super initWithCoder:aDecoder];
	if (self)	{
		self.layer.cornerRadius = 5;
		self.layer.masksToBounds = NO;
		self.layer.shadowOffset = CGSizeMake(0, 4);
		self.layer.shadowRadius = 10;
		self.layer.shadowOpacity = 0.5;
	}
	return self;
}

/*- (void)drawRect:(CGRect)rect {
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextSaveGState(currentContext);
    CGContextSetShadow(currentContext, CGSizeMake(-15, 20), 5);
    [super drawRect: rect];
    CGContextRestoreGState(currentContext);
}*/

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)btnMainMenuPressed:(id)sender {
	[(Ci_ViewController*)_viewController mainMenuPressed];
}

- (IBAction)btnRetryPressed:(id)sender {
	[(Ci_ViewController*)_viewController highscoreRetryPressed];
}

- (IBAction)btnTwitterPressed:(id)sender {
	[(Ci_ViewController*)_viewController btnTwitterPressed];
}

- (IBAction)btnFacebookPressed:(id)sender {
	[(Ci_ViewController*)_viewController btnFacebookPressed];
}
@end
