//
//  Ci_ViewPause.m
//  Circles
//
//  Created by Alexis on 09/10/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import "Ci_ViewPause.h"

#import "Ci_ViewController.h"

@implementation Ci_ViewPause

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder	{
	self = [super initWithCoder:aDecoder];
	if (self)	{
		self.layer.cornerRadius = 5;
		self.layer.masksToBounds = NO;
		self.layer.shadowOffset = CGSizeMake(0, 4);
		self.layer.shadowRadius = 10;
		self.layer.shadowOpacity = 0.5;
	}
	return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)btnMainmenuPressed:(id)sender {
	[(Ci_ViewController*)_viewController mainMenuPressed];
}

- (IBAction)btnBackPressed:(id)sender {
	[(Ci_ViewController*)_viewController pauseBackPressed];
}
@end
