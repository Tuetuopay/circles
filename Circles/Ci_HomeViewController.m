//
//  Ci_HomeViewController.m
//  Circles
//
//  Created by Alexis on 02/10/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import "Ci_HomeViewController.h"
#import "Ci_GameData.h"

@interface Ci_HomeViewController ()

@end

@implementation Ci_HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	_lblTitle.font = [UIFont fontWithName:@"JKAbode-LightDemo" size:64];
	_btnPlay.titleLabel.font = [UIFont fontWithName:@"JKAbode-LightDemo" size:40];
	_lblHighscore.font = [UIFont fontWithName:@"JKAbode-LightDemo" size:30];
	_btnReset.titleLabel.font = [UIFont fontWithName:@"JKAbode-LightDemo" size:30];
	
	_btnPlay.center = self.view.center;
}
-(void)viewDidAppear:(BOOL)animated	{
	[_lblHighscore setText:[NSString stringWithFormat:@"Highscore : %.2f%%", [Ci_GameData sharedInstance].highScore]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnPlayTouched:(id)sender {
	
}

- (IBAction)btnResetTouched:(id)sender {
	[Ci_GameData sharedInstance].highScore = 0.0;
	[[Ci_GameData sharedInstance] save];
	
	[self viewDidAppear:YES];	// Dirty, but it updates _lblHighscore's text
}
@end
