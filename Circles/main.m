//
//  main.m
//  Circles
//
//  Created by Alexis on 09/09/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Ci_AppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([Ci_AppDelegate class]));
	}
}
