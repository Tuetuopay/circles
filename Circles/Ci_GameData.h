//
//  Ci_GameData.h
//  Circles
//
//  Created by Alexis on 02/10/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Ci_GameData : NSObject <NSCoding>

@property (assign, nonatomic) double highScore;

+(instancetype)sharedInstance;
+(NSString*)filePath;
+(instancetype)loadInstance;
-(void)save;
-(instancetype)initWithCoder:(NSCoder *)aDecoder;
-(void)encodeWithCoder:(NSCoder *)aCoder;

@end
