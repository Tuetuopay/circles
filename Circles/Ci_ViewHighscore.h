//
//  Ci_ViewHighscore.h
//  Circles
//
//  Created by Alexis on 05/10/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ci_ViewHighscore : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblScore;
@property (weak, nonatomic) IBOutlet UIButton *btnMainMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnRetry;

@property (strong, nonatomic) UIViewController *viewController;

- (IBAction)btnMainMenuPressed:(id)sender;
- (IBAction)btnRetryPressed:(id)sender;
- (IBAction)btnTwitterPressed:(id)sender;
- (IBAction)btnFacebookPressed:(id)sender;

@end
