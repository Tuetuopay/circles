//
//  Ci_ViewPause.h
//  Circles
//
//  Created by Alexis on 09/10/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ci_ViewPause : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblPause;
@property (weak, nonatomic) IBOutlet UIButton *btnMainMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

- (IBAction)btnMainmenuPressed:(id)sender;
- (IBAction)btnBackPressed:(id)sender;

@property (strong, nonatomic) UIViewController *viewController;

@end
