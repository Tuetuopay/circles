//
//  Ci_DrawingView.h
//  Circles
//
//  Created by Alexis on 09/09/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ci_DrawingView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblSuccess;
@property (weak, nonatomic) IBOutlet UISwitch *switchDrawLines;
@property (weak, nonatomic) IBOutlet UIView *barSuccess;
@property (weak, nonatomic) IBOutlet UILabel *lblSuccessFG;
@property (weak, nonatomic) IBOutlet UIView *barSuccessBG;
@property (weak, nonatomic) IBOutlet UILabel *lblSuccessBG;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgBarWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fgBarWidth;

@property (strong, nonatomic) UIViewController *viewController;

@property float targetScore, shownScore;

- (void)restart;
- (void)setSuccessBar: (CGFloat)percentage;
- (void)updateDotPos;

- (void)makePerfectCircle: (CGFloat)radius;

- (void)computeScore;

- (void)timerTick;

@end
