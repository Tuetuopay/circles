//
//  Ci_ViewController.m
//  Circles
//
//  Created by Alexis on 09/09/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import "Ci_ViewController.h"
#import "Ci_GameData.h"

#import <Social/Social.h>

@interface Ci_ViewController ()

@end

@implementation Ci_ViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	_lblSuccessFG.font = [UIFont fontWithName:@"JKAbode-LightDemo" size:33];
	_lblSuccessBG.font = [UIFont fontWithName:@"JKAbode-LightDemo" size:33];
	
	_lblSuccessFG.text = @"";
	_lblSuccessBG.text = @"";
	
	_btnBGHighscore = [[UIButton alloc] initWithFrame:CGRectMake (0, 0, self.view.frame.size.width,self .view.frame.size.height)];
	[_btnBGHighscore addTarget:self action:@selector(highscoreRetryPressed) forControlEvents:UIControlEventTouchUpInside];
	_btnBGHighscore.hidden = YES;
	[self.view addSubview:_btnBGHighscore];
	
	_btnBGPause = [[UIButton alloc] initWithFrame:CGRectMake (0, 0, self.view.frame.size.width,self .view.frame.size.height)];
	[_btnBGPause addTarget:self action:@selector(pauseBackPressed) forControlEvents:UIControlEventTouchUpInside];
	_btnBGPause.hidden = YES;
	[self.view addSubview:_btnBGPause];
	
	_viewHighscore = [[[NSBundle mainBundle] loadNibNamed:@"ViewNewHighscore" owner:self options:nil] objectAtIndex:0];
	_viewHighscore.center = CGPointMake (_drawingView.center.x, _drawingView.center.y + 1000);
	_viewHighscore.lblTitle.font = [UIFont fontWithName:@"JKAbode-LightDemo" size:33];
	_viewHighscore.lblScore.font = [UIFont fontWithName:@"JKAbode-LightDemo" size:33];
	_viewHighscore.btnMainMenu.titleLabel.font = [UIFont fontWithName:@"JKAbode-LightDemo" size:33];
	_viewHighscore.btnRetry.titleLabel.font = [UIFont fontWithName:@"JKAbode-LightDemo" size:33];
	_viewHighscore.viewController = self;
	[_drawingView addSubview:_viewHighscore];
	
	_viewPause = [[[NSBundle mainBundle] loadNibNamed:@"ViewPause" owner:self options:nil] objectAtIndex:0];
	_viewPause.center = CGPointMake (_drawingView.center.x, _drawingView.center.y + 1000);
	_viewPause.lblPause.font = [UIFont fontWithName:@"JKAbode-LightDemo" size:33];
	_viewPause.btnMainMenu.titleLabel.font = [UIFont fontWithName:@"JKAbode-LightDemo" size:33];
	_viewPause.btnBack.titleLabel.font = [UIFont fontWithName:@"JKAbode-LightDemo" size:33];
	_viewPause.viewController = self;
	[_drawingView addSubview:_viewPause];
	
	_adViewBottomSpace.constant = -_adView.frame.size.height;
	[_drawingView layoutIfNeeded];
	_adShown = NO;
	_adView.delegate = self;
	[_drawingView updateDotPos];
	
	_drawingView.viewController = self;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)btnResartPressed:(id)sender {
	//[_drawingView makePerfectCircle:100];
	//[self.view addSubview:[[Ci_ViewHighscore alloc] initWithFrame:CGRectMake (20, 100, 270, 198)]];
	
	
	//UIView *mainView = [subviewArray objectAtIndex:0];
}

-(void)highscoreRetryPressed	{
	_btnBGHighscore.hidden = YES;
	[UIView animateWithDuration:0.3
						  delay:0
						options:UIViewAnimationCurveEaseOut
					 animations:^ {
						 _viewHighscore.center = CGPointMake (_drawingView.center.x, _drawingView.center.y + 100);
						 _viewHighscore.alpha = 0.0;
					 }completion:^(BOOL finished) {
						 _viewHighscore.center = CGPointMake (_drawingView.center.x, _drawingView.center.y + 1000);
					 }];
}
-(void)pauseBackPressed	{
	_btnBGPause.hidden = YES;
	[UIView animateWithDuration:0.3
						  delay:0
						options:UIViewAnimationCurveEaseOut
					 animations:^ {
						 _viewPause.center = CGPointMake (_drawingView.center.x, _drawingView.center.y + 100);
						 _viewPause.alpha = 0.0;
					 }completion:^(BOOL finished) {
						 _viewPause.center = CGPointMake (_drawingView.center.x, _drawingView.center.y + 1000);
					 }];
}

- (IBAction)btnPausedPressed:(id)sender {
	_btnBGPause.hidden = NO;
	_viewPause.center = CGPointMake (_drawingView.center.x, _drawingView.center.y + 100);
	_viewPause.alpha = 0.0;
	[UIView animateWithDuration:0.3
						  delay:0
						options:UIViewAnimationCurveEaseOut
					 animations:^ {
						 _viewPause.center = _drawingView.center;
						 _viewPause.alpha = 1.0;
					 }completion:^(BOOL finished) {
					 }];
}
-(void)mainMenuPressed	{
	[self dismissViewControllerAnimated:YES completion:^{}];
}

-(void)newHighscore:(double)score	{
	UIGraphicsBeginImageContext (_drawingView.bounds.size);
	[_drawingView.layer renderInContext:UIGraphicsGetCurrentContext()];
	_imgScreenshot = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	_btnBGHighscore.hidden = NO;
	
	_viewHighscore.center = CGPointMake (_drawingView.center.x, _drawingView.center.y + 100);
	_viewHighscore.alpha = 0.0;
	_viewHighscore.lblScore.text = [NSString stringWithFormat:@"%.2f %%", score];
	[UIView animateWithDuration:0.4
						  delay:0
						options:UIViewAnimationCurveEaseOut
					 animations:^ {
						 _viewHighscore.center = _drawingView.center;
						 _viewHighscore.alpha = 1.0;
					 }completion:^(BOOL finished) {
					 }];
}
- (void)btnTwitterPressed	{
	if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])	{
		SLComposeViewController *tweetSheet = [SLComposeViewController
											   composeViewControllerForServiceType:SLServiceTypeTwitter];
		[tweetSheet setInitialText:[NSString stringWithFormat:@"I scored %.2f%% on @Circles_game !", [Ci_GameData sharedInstance].highScore]];
		[tweetSheet addImage:_imgScreenshot];
	
		[self presentViewController:tweetSheet animated:YES completion:nil];
	}
	else	{
		[[[UIAlertView alloc] initWithTitle:@"Sharing failed"
									message:@"Unable to share your score through Twitter, please check in your Settings that Twitter is enabled."
								   delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
	}
}
- (void)btnFacebookPressed	{
	if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])	{
		SLComposeViewController *fbSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
		
		[fbSheet setInitialText:[NSString stringWithFormat:@"I scored %.2f%% on Circles_game !", [Ci_GameData sharedInstance].highScore]];
		[fbSheet addImage:_imgScreenshot];
		
		[self presentViewController:fbSheet animated:YES completion:Nil];
	}
	else	{
		[[[UIAlertView alloc] initWithTitle:@"Sharing failed"
									message:@"Unable to share your score through Facebook, please check in your Settings that Facebook is enabled."
								   delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
	}
}

// Ads
-(BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave	{
	return YES;
}
-(void)bannerViewDidLoadAd:(ADBannerView *)banner	{
	if (_adShown)
		return;
	[_drawingView layoutIfNeeded];
	if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)	{
		[UIView animateKeyframesWithDuration:0.2 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeCubic animations:^{
			_adViewBottomSpace.constant = 0;
			[_drawingView layoutIfNeeded];
		}completion:^(BOOL finished){
			_adShown = YES;
		}];
	}
	else	{
		_adViewBottomSpace.constant = 0;
		[_drawingView layoutIfNeeded];
		_adShown = YES;
	}
}
-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error	{
	if (!_adShown)
		return;
	[_drawingView layoutIfNeeded];
	if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)	{
		[UIView animateKeyframesWithDuration:0.2 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeCubic animations:^{
			_adViewBottomSpace.constant = -_adView.frame.size.height;
			[_drawingView layoutIfNeeded];
		}completion:^(BOOL finished){
			_adShown = NO;
		}];
	}
	else	{
		_adViewBottomSpace.constant = -_adView.frame.size.height;
		[_drawingView layoutIfNeeded];
		_adShown = NO;
	}
}

@end





