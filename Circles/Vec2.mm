//
//  Vec2.cpp
//  Circles
//
//  Created by Alexis on 25/09/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#include "Vec2.h"

#include <cmath>

Vec2::Vec2 ()	{
	x = y = 0;
}
Vec2::Vec2 (double cx, double cy)	{
	x = cx;	y = cy;
}
Vec2::Vec2 (CGPoint vec)	{
	x = vec.x;	y = vec.y;
}
Vec2::Vec2 (CGPoint from, CGPoint to)	{
	x = to.x - from.x;
	y = to.y - from.y;
}

double Vec2::dot (const Vec2 &vec) const	{
	return x * vec.x + y * vec.y;
}
double Vec2::cross (const Vec2 &vec) const	{
	return x * vec.y - y * vec.x;
}
double Vec2::length ()	{
	return sqrt (x*x + y*y);
}
void Vec2::normalize ()	{
	x /= length ();
	y /= length ();
}
Vec2 Vec2::normalized () const	{
	Vec2 v (x, y);
	v.normalize ();
	return v;
}
CGPoint Vec2::toCGPoint () const	{
	return CGPointMake (x, y);
}

Vec2 Vec2::operator+ (const Vec2& v) const	{
	return Vec2 (x + v.x, y + v.y);
}
Vec2 Vec2::operator- (const Vec2& v) const	{
	return Vec2 (x - v.x, y - v.y);
}
Vec2 Vec2::operator* (const double& v) const	{
	return Vec2 (x * v, y * v);
}
Vec2 Vec2::operator/ (const double& v) const	{
	return Vec2 (x / v, y / v);
}
void Vec2::operator+= (const Vec2& v)	{
	x += v.x;	y += v.y;
}
void Vec2::operator-= (const Vec2& v)	{
	x -= v.x;	y -= v.y;
}
void Vec2::operator*= (const double& v)	{
	x *= v;	y *= v;
}
void Vec2::operator/= (const double& v)	{
	x /= v;	y /= v;
}

Vec2::~Vec2 ()	{
	
}