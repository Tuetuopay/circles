//
//  Ci_ViewController.h
//  Circles
//
//  Created by Alexis on 09/09/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

#import "Ci_DrawingView.h"
#import "Ci_ViewHighscore.h"
#import "Ci_ViewPause.h"

@interface Ci_ViewController : UIViewController <ADBannerViewDelegate>

@property (strong, nonatomic) IBOutlet Ci_DrawingView *drawingView;
@property (weak, nonatomic) IBOutlet UILabel *lblSuccess;
@property (weak, nonatomic) IBOutlet UILabel *lblSuccessFG;
@property (weak ,nonatomic) IBOutlet UILabel *lblSuccessBG;
@property (weak, nonatomic) IBOutlet ADBannerView *adView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *adViewBottomSpace;

@property (strong, nonatomic) Ci_ViewHighscore *viewHighscore;
@property (strong, nonatomic) Ci_ViewPause	*viewPause;

@property UIImage *imgScreenshot;

@property (strong, nonatomic) UIButton *btnBGPause;
@property (strong, nonatomic) UIButton *btnBGHighscore;

@property BOOL adShown;

-(void)highscoreRetryPressed;
-(void)mainMenuPressed;
-(void)pauseBackPressed;
- (IBAction)btnPausedPressed:(id)sender;

-(void)newHighscore:(double)score;

- (void)btnTwitterPressed;
- (void)btnFacebookPressed;

@end
