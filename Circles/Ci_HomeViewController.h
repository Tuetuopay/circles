//
//  Ci_HomeViewController.h
//  Circles
//
//  Created by Alexis on 02/10/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ci_HomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
@property (weak, nonatomic) IBOutlet UILabel *lblHighscore;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;

- (IBAction)btnPlayTouched:(id)sender;
- (IBAction)btnResetTouched:(id)sender;

@end
