//
//  Ci_DrawingView.m
//  Circles
//
//  Created by Alexis on 09/09/2014.
//  Copyright (c) 2014 tuetuopay. All rights reserved.
//

#import "Ci_DrawingView.h"
#import "Ci_GameData.h"

#import "Ci_ViewController.h"

#include "Vec2.h"

#include <vector>

/*CGFloat fabs (CGFloat a)	{
	return a > 0 ? a : -a;
}*/

@implementation Ci_DrawingView	{
	UIBezierPath *path, *middleDot, *lines, *avgCircle;
	int lastIndex;
	
	CGPoint curPts[4], lastAcceptedPoint;
	int curPt;
	
	CATransition *animation;
}

- (id)initWithCoder:(NSCoder *)aDecoder // (1)
{
	if (self = [super initWithCoder:aDecoder])	{
		[self setMultipleTouchEnabled:NO]; // (2)
		[self setBackgroundColor:[UIColor whiteColor]];
		middleDot = [UIBezierPath bezierPath];
		[middleDot setLineWidth:1.0];
		[middleDot addArcWithCenter:self.center radius:1.0 startAngle:0 endAngle:2*M_PI clockwise:true];
		[self restart];
		[self setSuccessBar:0];
		
		_shownScore = _targetScore = 0.0;
		
		NSTimer *tUpdate;
		NSTimeInterval tiCallRate = 1.0 / 60.0;
		tUpdate = [NSTimer scheduledTimerWithTimeInterval:tiCallRate
												   target:self
												 selector:@selector(timerTick)
												 userInfo:nil
												  repeats:YES];
		
		animation = [CATransition animation];
		animation.duration = 0.4;
		animation.type = kCATransitionFromBottom;
		animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
		
		[_lblSuccessBG.layer addAnimation:animation forKey:@"changeTextTransition"];
		[_lblSuccessFG.layer addAnimation:animation forKey:@"chengeTextTransition"];
	}
	return self;
}
- (void)updateDotPos	{
	middleDot = [UIBezierPath bezierPath];
	[middleDot setLineWidth:1.0];
	[middleDot addArcWithCenter:self.center radius:1.0 startAngle:0 endAngle:2*M_PI clockwise:true];
	[self restart];
	[self setSuccessBar:0];
}

- (void)drawRect:(CGRect)rect	{	// (5)
	[[UIColor blackColor] setStroke];
	[path stroke];
	if (_switchDrawLines.isOn)
		[lines stroke];
	[[UIColor greenColor] setStroke];
	[avgCircle stroke];
	[[UIColor redColor] setStroke];
	[[UIColor redColor] setFill];
	[middleDot stroke];
}

/*CGFloat distanceBetween (CGPoint a, CGPoint b)	{
	return sqrt ((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
}
CGFloat crossProduct (CGPoint vecA, CGPoint vecB)	{
	return (vecA.x * vecB.y) - (vecA.y * vecB.x);
}*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event	{
	UITouch *touch = [touches anyObject];
	CGPoint p = [touch locationInView:self];
	[self restart];
	[path moveToPoint:p];
	lastAcceptedPoint = p;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event	{
	UITouch *touch = [touches anyObject];
	CGPoint p = [touch locationInView:self];
	
	if (Vec2 (lastAcceptedPoint, p).length () <= 1)
		return;
	lastAcceptedPoint = p;
	curPts[++curPt] = p;
	if (curPt == 3)	{
		[path moveToPoint:curPts[0]];
		[path addCurveToPoint:curPts[3] controlPoint1:curPts[1] controlPoint2:curPts[2]];
		[self setNeedsDisplay];
		curPts[0] = [path currentPoint];
		curPt = 0;
	}
	//[path addLineToPoint:p];
	[self setNeedsDisplay];
	
	// [_lblSuccess setText:[NSString stringWithFormat:@"View center : %f,%f\nFinger pos : %f,%f", self.center.x, self.center.y, p.x, p.y]];
}

void pathApplier (void *info, const CGPathElement *element)	{
	NSMutableArray *bezierPoints = (__bridge NSMutableArray*)info;
	CGPoint *points = element->points;
	CGPathElementType type = element->type;
	switch (type) {
		case kCGPathElementMoveToPoint:
		case kCGPathElementAddLineToPoint:
			[bezierPoints addObject:[NSValue valueWithCGPoint:points[0]]];
			break;
		case kCGPathElementAddQuadCurveToPoint:
			[bezierPoints addObject:[NSValue valueWithCGPoint:points[0]]];
			[bezierPoints addObject:[NSValue valueWithCGPoint:points[1]]];
			break;
		case kCGPathElementAddCurveToPoint:
			[bezierPoints addObject:[NSValue valueWithCGPoint:points[0]]];
			[bezierPoints addObject:[NSValue valueWithCGPoint:points[1]]];
			[bezierPoints addObject:[NSValue valueWithCGPoint:points[2]]];
			break;
		case kCGPathElementCloseSubpath:
			break;
			
		default:
			break;
	}
}
void pathApplierStl (void *info, const CGPathElement *element)	{
	std::vector<CGPoint> *bezierPoints = (std::vector<CGPoint>*)info;
	CGPoint *points = element->points;
	CGPathElementType type = element->type;
	switch (type) {
		case kCGPathElementMoveToPoint:
		case kCGPathElementAddLineToPoint:
			bezierPoints->push_back (points[0]);
			break;
		case kCGPathElementAddQuadCurveToPoint:
			bezierPoints->push_back (points[0]);
			bezierPoints->push_back (points[1]);
			break;
		case kCGPathElementAddCurveToPoint:
			bezierPoints->push_back (points[0]);
			bezierPoints->push_back (points[1]);
			bezierPoints->push_back (points[2]);
			break;
		case kCGPathElementCloseSubpath:
			break;
			
		default:
			break;
	}
}
/*CGFloat length (CGPoint a)	{
	return sqrt (a.x*a.x + a.y*a.y);
}*/

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event	{
	[self touchesMoved:touches withEvent:event];
	
	[self computeScore];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event	{
	[self touchesEnded:touches withEvent:event];
}

- (void)restart	{
	path = [UIBezierPath bezierPath];
	[path setLineWidth:2.0];
	// [path moveToPoint:CGPointMake (self.center.x, self.center.y)];
	// [path addLineToPoint:CGPointMake (self.center.x, self.center.y)];
	lines = [UIBezierPath bezierPath];
	[lines setLineWidth:1.0];
	avgCircle = [UIBezierPath bezierPath];
	[avgCircle setLineWidth:2.0];
	[self setNeedsDisplay];
	
	middleDot = [UIBezierPath bezierPath];
	[middleDot setLineWidth:2.0];
	[middleDot addArcWithCenter:self.center radius:1.0 startAngle:0 endAngle:2*M_PI clockwise:true];
	
	lastIndex = 0;
	curPt = -1;
	lastAcceptedPoint.x = lastAcceptedPoint.y = 0;
}

-(void)setSuccessBar:(CGFloat)percentage	{
	// Animating the score text
	/* CATransition *animation = [CATransition animation];
	animation.duration = 0.4;
	animation.type = kCATransitionFromBottom;
	animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	[_lblSuccessBG.layer addAnimation:animation forKey:@"changeTextTransition"];
	[_lblSuccessFG.layer addAnimation:animation forKey:@"chengeTextTransition"];
	
	[_lblSuccessBG setText:[NSString stringWithFormat:@"%.2f %%", percentage]];
	[_lblSuccessFG setText:[NSString stringWithFormat:@"%.2f %%", percentage]]; */
	
	_targetScore = percentage;
	
	CGRect newFrame1 = _barSuccess.frame;
	newFrame1.size.width = percentage * self.frame.size.width / 100.0;
	CGRect newFrame2 = _barSuccessBG.frame;
	newFrame2.size.width = percentage * self.frame.size.width / 100.0;
	
	
	[self layoutIfNeeded];
	[UIView animateWithDuration:0.4
						  delay:0
						options:UIViewAnimationCurveEaseInOut
					 animations:^ {
						 _fgBarWidth.constant = percentage * self.frame.size.width / 100.0;
						 _bgBarWidth.constant = percentage * self.frame.size.width / 100.0;
						 [self layoutIfNeeded];
					 }completion:^(BOOL finished) {
					 }];
}

-(void)timerTick	{
	if (_shownScore != _targetScore)	{
		if (fabs (_shownScore - _targetScore) <= 0.1)
			_shownScore = _targetScore;
		else
			_shownScore += (_targetScore - _shownScore) / 3.0;
		
		[_lblSuccessBG setText:[NSString stringWithFormat:@"%.2f %%", _shownScore]];
		[_lblSuccessFG setText:[NSString stringWithFormat:@"%.2f %%", _shownScore]];
	}
}

-(void)makePerfectCircle:(CGFloat)radius	{
	CGFloat pointCount = 100;
	// [self restart];
	[path moveToPoint:self.center];
	for (CGFloat theta = 0.0; theta <= 2*M_PI; theta += 2*M_PI / pointCount)	{
		[path addLineToPoint:CGPointMake (self.center.x + cos (theta) * radius, self.center.y + sin (theta) * radius)];
		[self setNeedsDisplay];
	}
	
	[self computeScore];
}
-(void)computeScore	{
	CGPathRef cgPath = path.CGPath;
	std::vector<CGPoint> points;
	// NSMutableArray *points = [NSMutableArray array];
	//CGPathApply (cgPath, (__bridge void *)(points), pathApplier);
	CGPathApply (cgPath, (void*)&points, pathApplierStl);
	if (points.size () - lastIndex < 10)	{
		[self restart];
		return;
	}
	
	CGFloat minDist = -1.0;
	int lowIndex = -1, highIndex = -1;
	int crosses = 0;
	for (int i = 0; i < points.size ()-1; i++)
		for (int j = 0; j < points.size ()-1; j++)	{
			if (i == j)	continue;
			
			Vec2 seg1 (points[i], points[i+1]), seg2 (points[j], points[j+1]);
			
			float sgn1 = seg1.cross (Vec2 (points[i], points[j])) * seg1.cross (Vec2 (points[i], points[j+1])),
				sgn2 = seg1.cross (Vec2 (points[j], points[i+1])) * seg1.cross (Vec2 (points[j], points[i+1]));
			
			if (sgn1 * sgn2 > 0 || sgn1 < 0)	{
				crosses++;
			}
		}
	/*for (int i = 0; i < points.size (); i++)	{
		if (i <= lowIndex || i >= highIndex)
			points.erase (points.);
			[points removeObjectAtIndex:i];
	}*/
	if (lowIndex != -1)
		points.erase (points.begin (), points.begin () + lowIndex);
	if (highIndex != -1)
		points.erase (points.begin () + highIndex - ((lowIndex != -1) ? lowIndex : 0), points.end ());
	
	CGFloat avg = 0, avgDist = 0, avgDist2 = 0, absAngle = 0, totAngle = 0, totalLength = 0;
	/* for (int i = lastIndex; i < points.count - 1; i++)	{
	 Ox += [(NSValue*)[points objectAtIndex:i] CGPointValue].x;
	 Oy += [(NSValue*)[points objectAtIndex:i] CGPointValue].y;
	 }
	 Ox /= (points.count - 1 - lastIndex);
	 Oy /= (points.count - 1 - lastIndex); */
	Vec2 O (self.center);
	Vec2 lastiVec, vU;
	for (int i = lastIndex; i < points.size () - 1; i++)	{
		Vec2 pA (points[i]), pB (points[i+1]),
			 pM = (pA + pB) / 2.0,
			 vV = pB - pA;
		Vec2 iVec (pA.y - pB.y, pB.x - pA.x);
		// CGPoint iVec = CGPointMake (pA.y - pB.y, pB.x - pA.x);
		CGFloat len = iVec.length ();
		if (len > 0)
			iVec /= len;
		
		// l = length (CGPointMake (pA.x - O.x, pA.y - O.y)) * length (CGPointMake (pB.x - O.x, pB.y - O.y));
		avg += (pA - O).length ();
		CGFloat l = (iVec.dot (lastiVec)) / (iVec.length ()* lastiVec.length ());
		if (l >= -1 && l <= 1)	{
			absAngle += acos (l);
			totAngle += acos (l) * ((vU.cross (vV - vU) > 0.0) ? 1.0 : -1.0);
			// totAngle += acos (l) * (vU.cross (vV - vU) > 0 (vU.x * (vV.y - vU.y) > vU.y * (vV.x - vU.x)) ? 1.0 : -1.0);
		}
		
		if (_switchDrawLines.isOn)	{
			[lines moveToPoint:(pM - iVec * 100).toCGPoint ()];
			[lines addLineToPoint:(pM - iVec * 100).toCGPoint ()];
		}
		l = iVec.length ();
		if (l != 0)
			avgDist += fabs ((pM - O).cross (iVec) / l) * fabs ((pM - O).cross (iVec) / l);
		// avgDist += fabs (((pM.x - O.x) * iVec.y - (pM.y - O.y) * iVec.x) / l) * fabs (((pM.x - O.x) * iVec.y - (pM.y - O.y) * iVec.x) / l);
		avgDist2 += sqrt ((pM - O).length ()*(pM - O).length () + iVec.dot (pM - O)*iVec.dot (pM - O));
		
		totalLength += (pB - pA).length ();
		
		// lastiVec = CGPointMake (pB.x - pA.x, pB.y - pA.y);
		lastiVec = iVec;
		vU = vV;
	}
	avg += Vec2 (O.toCGPoint (), points.back ()).length ();
	avg /= points.size () - lastIndex;
	avgDist /= (points.size () - 1 - lastIndex);
	avgDist /= avg;
	avgDist2 /= (points.size () - 1 - lastIndex);
	
	lastIndex = points.size ();
	// avgDist *= 2;
	
	absAngle *= (180.0 / M_PI);
	totAngle *= (180.0 / M_PI);
	if (//absAngle <= 200 || absAngle >= 300 || // Circle is too big / small
		absAngle * 0.7 >= fabs (totAngle) || absAngle * 1.3 <= fabs (totAngle))	{ // We did a curve
		[self restart];
		//return;
	}
	if (totalLength < 0.85 * 2.0 * M_PI * avg || totalLength > 1.2 * 2.0 * M_PI * avg);
		//[self restart];
	
	double avgDistPercent = (1 - (avgDist * 3)) * 100;
	avgDistPercent = (avgDistPercent < 0) ? 0 : avgDistPercent;
	
	double score = 100.0 * exp (-avgDist / 15.0);
	
	[avgCircle addArcWithCenter:O.toCGPoint () radius:avg startAngle:0 endAngle:2*M_PI clockwise:true];
	[_lblSuccess setText:@""];
	//[_lblSuccess setText:
	// [NSString stringWithFormat:@"Angle : %f °\nCrosses : %d\nLen : %f\nLen2 : %f\nRatio : %f",
	//	absAngle, crosses, 2 * M_PI * avg, totalLength, 2 * M_PI * avg / totalLength]];
	// (1 - avgDist) * 100
	[self setSuccessBar:score];
	
	if (score >= [Ci_GameData sharedInstance].highScore)	{
		[Ci_GameData sharedInstance].highScore = score;
		[[Ci_GameData sharedInstance] save];
		[(Ci_ViewController*)_viewController newHighscore:score];
	}
	
	[self setNeedsDisplay];
}

@end
